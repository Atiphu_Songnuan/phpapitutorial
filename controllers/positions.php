<?php
class Positions extends Controller
{
    public function __construct()
    {
        parent::__construct('positions');
        $result = $this->model->GetAllPositions();
        $result = array(
            "data" => json_decode($result),
        );
        echo json_encode($result);
    }
}

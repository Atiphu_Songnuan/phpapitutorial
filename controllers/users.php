<?php
class Users extends Controller
{
    public function __construct()
    {
        parent::__construct('users');    
        $result = $this->model->GetAllUsers();
        $result = array(
            "data"=> json_decode($result)
        );
        echo json_encode($result);    
    }
}

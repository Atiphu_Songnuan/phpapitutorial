<?php
class Positions_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAllPositions()
    {
        $sql = "SELECT * FROM position";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $jsonData = json_encode($data);
        return $jsonData;
    }
}

<?php
class Users_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAllUsers()
    {
        $sql = "SELECT * FROM users";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        $jsonData = json_encode($data);
        return $jsonData;
    }
}
